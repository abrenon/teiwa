{-# LANGUAGE NamedFieldPuns #-}
module CLI (
      Command(..)
    , getCommand
  ) where

import Data.Text.Lazy as Text (Text, split)
import Data.Version (showVersion)
import Control.Applicative ((<*>), optional)
import Options.Applicative (
      Parser, ReadM, strArgument, execParser, fullDesc, header, help, helper
    , info, long, metavar, option, short, str, switch, value
  )
import qualified Paths_teiwa as Teiwa (version)
import Text.TEIWA (Config(..), defaultConfig)

data Command = Command {
      annotationsFile :: FilePath
    , config :: Config
  }

charPredicate :: ReadM (Char -> Bool)
charPredicate = flip elem <$> (str :: ReadM String)

csv :: ReadM [Text]
csv = split (== ',') <$> str

configOptions :: Parser Config
configOptions = Config
  <$> option (optional str) (
            short 'c' <> long "formColumn" <> value formColumn
          <> metavar "COLUMN_NAME"
          <> help "the column to use as the form"
        )
  <*> option (optional csv) (short 'H' <> long "header" <> value headerOverride
          <> metavar "COLUMN_NAMES"
          <> help "comma-separated names to use for the columns (can be empty to unselect a column)"
        )
  <*> option charPredicate (short 'p' <> long "punctuation" <> value punctuation
          <> metavar "PUNCTUATION_CHARACTERS"
          <> help "characters to encode as punctuation (defaults to Data.Char.isPunctuation)"
        )
  <*> switch (short 's' <> long "strictTEI"
          <> help "only use TEI's att.linguistic on the elements"
        )
  where
    Config {formColumn, headerOverride, punctuation} = defaultConfig

command :: Parser Command
command = Command
  <$> strArgument (metavar "ANNOTATIONS" <> help "annotations to apply")
  <*> configOptions

getCommand :: IO Command
getCommand = execParser $
  info
    (helper <*> command)
    (fullDesc <> header ("teiwa v" ++ showVersion Teiwa.version))
