{-# LANGUAGE NamedFieldPuns #-}
module Main where

import CLI (Command(..), getCommand)
import Control.Monad.Except (ExceptT(..), runExceptT)
import Data.Text.Lazy as Text (Text)
import Data.Text.Lazy.IO as Text (getContents, putStr)
import Text.TEIWA (Error, Origin(..), Source(..), annotateWith, coNLLX, csv, tsv)
import System.FilePath (takeExtension)
import System.Exit (die)

annotator :: Command -> Text -> ExceptT Error IO Text
annotator (Command {annotationsFile, config}) =
  annotateWith config $ Source format (File annotationsFile)
  where
    extension = takeExtension annotationsFile
    format
      | extension == ".csv" = csv
      | extension == ".tsv" = tsv
      | otherwise = coNLLX

main :: IO ()
main = do
  command <- getCommand
  input <- Text.getContents
  runExceptT (annotator command input) >>= either (die . show) Text.putStr
