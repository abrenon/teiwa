{-# LANGUAGE FlexibleContexts #-}
module Text.TEIWA (
      module Annotation
    , module Text.TEIWA.Config
    , module Text.TEIWA.Error
    , module Source
    , annotate
    , annotateWith
    , fromCSV
    , fromCoNLLX
  ) where

import Control.Monad.Except (MonadError(..))
import Control.Monad.IO.Class (MonadIO(..))
import Data.Text.Lazy as Text (Text)
import Text.TEIWA.Annotation as Annotation
import Text.TEIWA.Config
import Text.TEIWA.Error
import Text.TEIWA.Source as Source

type Filter m = Text -> m Text

annotateWith :: (MonadError Error m, MonadIO m) =>
  Config -> Source -> Filter m
annotateWith config source input = do
  annotation <- Source.parse config source
  Annotation.apply config annotation input

annotate :: (MonadError Error m, MonadIO m) => Source -> Filter m
annotate = annotateWith defaultConfig

fromCSV :: (MonadError Error m, MonadIO m) => Origin -> Filter m
fromCSV = annotate . Source csv

fromCoNLLX :: (MonadError Error m, MonadIO m) => Origin -> Filter m
fromCoNLLX = annotate . Source coNLLX
