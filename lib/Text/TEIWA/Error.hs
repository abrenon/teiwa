module Text.TEIWA.Error (
    Error(..)
  ) where

import Text.Parsec (Line, ParseError)
import Text.Printf (printf)

data Error =
    NoSuchColumn String
  | NoFormColumn
  | EmptyHeader
  | MissingColumn Line String
  | ParsingError ParseError
  | TermNotFound String

instance Show Error where
  show (NoSuchColumn s) =
    printf "\"%s\" isn't a valid column name in this file" s
  show NoFormColumn =
    "No \"form\" column has been found, select one with --formColumn"
  show EmptyHeader = "The CSV file header is empty"
  show (MissingColumn l s) = printf "Line %d is missing a value for column %s" l s
  show (ParsingError e) = show e
  show (TermNotFound t) = printf "Annotated term \"%s\" wasn't found in the input" t
