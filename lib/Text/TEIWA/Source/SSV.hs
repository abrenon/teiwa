{-# LANGUAGE FlexibleContexts #-}
module Text.TEIWA.Source.SSV (
      body
    , header
  ) where

import Control.Applicative ((<|>), many)
import Control.Monad.Except (MonadError(..))
import Data.Maybe (catMaybes)
import Data.Text.Lazy as Text (pack)
import Text.Parsec (ParsecT, Stream, between, char, noneOf, sepBy, string, try)
import Text.TEIWA.Source.Common (Field, Header, Row, TEIWAParser, eol, recordLine)
import Text.TEIWA.Error (Error(..))

field :: Stream s m Char => Char -> ParsecT s u m Field
field separator = notEmpty <$> (regular <|> quoted)
  where
    notEmpty "" = Nothing
    notEmpty s = Just $ Text.pack s
    regular = many (noneOf $ separator:"\n\r\"")
    quoted = between quote quote $
      many (noneOf "\"" <|> try (string "\"\"" *> pure '"'))
    quote = char '"'

fields :: Stream s m Char => Char -> ParsecT s u m [Field]
fields separator = (field separator `sepBy` char separator) <* eol

header :: Char -> TEIWAParser Header
header separator = fields separator >>= ensureNotEmpty . catMaybes
  where
    ensureNotEmpty [] = throwError EmptyHeader
    ensureNotEmpty l = return l

body :: Stream s m Char => Char -> ParsecT s u m [Row]
body = many . recordLine . fields
