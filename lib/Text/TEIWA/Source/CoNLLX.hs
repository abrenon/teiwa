{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
module Text.TEIWA.Source.CoNLLX (
      header
    , sentences
  ) where

import Control.Applicative ((<|>), many)
import Data.Text.Lazy as Text (concat, pack)
import Text.Parsec (ParsecT, Stream, char, digit, many1, noneOf, string, try)
import Text.TEIWA.Source.Common (Field, Header, Row, eol, recordLine)

header :: Header
header = [
      "ID"
    , "FORM"
    , "LEMMA"
    , "CPOSTAG"
    , "POSTAG"
    , "FEATS"
    , "HEAD"
    , "DEPREL"
    , "PHEAD"
    , "PDEPREL"
  ]

field :: Stream s m Char => ParsecT s u m Field
field = build <$> many1 (noneOf "\t\n\r")
  where
    build "_" = Nothing
    build s = Just $ Text.pack s

type Range = (Int, Int)

rowID :: Stream s m Char => ParsecT s u m (Either Range Int)
rowID = (Left <$> try ((,) <$> int <* char '-' <*> int)) <|> (Right <$> int)
  where
    int = read <$> many1 digit

row :: Stream s m Char => ParsecT s u m Row
row = recordLine (rowID >>= either range singleLine)
  where
    toText = Text.pack . show
    rest = many (char '\t' *> field) <* eol
    singleLine n = (Just (toText n):) <$> rest
    range (from, to) =
      let rangeID = Text.concat [toText from, "-", toText to] in do
      main <- rest
      subs <- mapM (\k -> string (show k) *> rest) [from .. to]
      return $ (Just rangeID):(zipWith (<|>) main $ foldl1 combineSubs subs)
    combineSubs = zipWith combineFields
    combineFields Nothing Nothing = Nothing
    combineFields Nothing f@(Just _) = f
    combineFields f@(Just _) Nothing = f
    combineFields (Just a) (Just b) = Just $ Text.concat [a, "+", b]

type Sentence = [Row]

sentence :: Stream s m Char => ParsecT s u m Sentence
sentence = many comment *> many1 row
  where
    comment = char '#' *> many (noneOf "\r\n") <* eol

sentences :: Stream s m Char => ParsecT s u m [Sentence]
sentences = many (sentence <* many1 eol)
