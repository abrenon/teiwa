{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
module Text.TEIWA.Annotation (
      Annotation(..)
    , Attributes
    , SentenceAnnotation(..)
    , TokenAnnotation(..)
    , apply
  ) where

import Control.Monad.Except (MonadError(..))
import Control.Monad.RWS (RWST, evalRWST, get, put, reader, tell)
import Data.Text.Lazy as Text (Text, breakOn, concat, drop, head, length, unpack)
import Text.TEIWA.Config (Config(..))
import Text.TEIWA.Error (Error(..))

type Attributes = [(Text, Text)]

data TokenAnnotation = TokenAnnotation {
      form :: Text
    , annotated :: Attributes
  } deriving Show

newtype SentenceAnnotation = SentenceAnnotation {
    getTokens :: [TokenAnnotation]
  }

data Annotation =
    SentenceLevel [SentenceAnnotation]
  | TokenLevel [TokenAnnotation]

attribute :: (Text, Text) -> Text
attribute (k, v) = Text.concat [" ", k, "=\"", v, "\""]

token :: Text -> TokenAnnotation -> Text
token elementName (TokenAnnotation {form, annotated}) =
  Text.concat (openTag ++ (form:closeTag))
  where
    openTag = ("<":elementName:(attribute <$> annotated) ++ [">"])
    closeTag = ["</", elementName,">"]

type Editor m = RWST Config Text Text m ()

pc :: TokenAnnotation -> Text
pc = token "pc"

w :: TokenAnnotation -> Text
w = token "w"

check :: MonadError Error m => Maybe Text -> TokenAnnotation -> (Text, Text) -> Editor m
check openingTag expected@(TokenAnnotation {form}) (before, focused) =
  if Text.length focused >= expectedLength
  then do
    tagged <- reader (handle . punctuation)
    put (Text.drop expectedLength focused)
    mapM_ tell [before, maybe "" id openingTag, tagged]
  else throwError (TermNotFound $ Text.unpack form)
  where
    expectedLength = Text.length form
    handle f = (if expectedLength == 1 && f (Text.head form) then pc else w) expected

tokenLevel :: MonadError Error m => Maybe Text -> [TokenAnnotation] -> Editor m
tokenLevel Nothing [] = pure ()
tokenLevel (Just t) [] = tell t
tokenLevel openingTag (annotation@(TokenAnnotation {form}):others) =
  ((Text.breakOn form <$> get) >>= check openingTag annotation)
    *> (tokenLevel Nothing others)

sentenceLevel :: MonadError Error m => SentenceAnnotation -> Editor m
sentenceLevel sentenceAnnotation =
  tokenLevel (Just "<s>") (getTokens sentenceAnnotation) *> tell "</s>"

apply :: MonadError Error m => Config -> Annotation -> Text -> m Text
apply config annotation =
  fmap snd . evalRWST (terms annotation *> get >>= tell) config
  where
    terms (SentenceLevel sentences) = mapM_ sentenceLevel sentences
    terms (TokenLevel tokens) = tokenLevel Nothing tokens
