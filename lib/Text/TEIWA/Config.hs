module Text.TEIWA.Config (
      Config(..)
    , defaultConfig
  ) where

import Data.Char (isPunctuation)
import Data.Text.Lazy (Text)

data Config = Config {
      formColumn :: Maybe Text
    , headerOverride :: Maybe [Text]
    , punctuation :: Char -> Bool
    , strictTEI :: Bool
  }

defaultConfig :: Config
defaultConfig = Config {
      formColumn = Nothing
    , headerOverride = Nothing
    , punctuation = isPunctuation
    , strictTEI = False
  }
